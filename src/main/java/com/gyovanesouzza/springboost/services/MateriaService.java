package com.gyovanesouzza.springboost.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gyovanesouzza.springboost.Repository.MateriaRepository;
import com.gyovanesouzza.springboost.beans.Materia;
import com.gyovanesouzza.springboost.beans.dto.MateriaDTO;
import com.gyovanesouzza.springboost.exceptions.ObjectNotFoundException;

@Service
public class MateriaService {

	@Autowired
	private MateriaRepository mateiraRepository;

	public List<Materia> findAll() {
		return mateiraRepository.findAll();
	}

	public Materia findById(Integer id) {

		Optional<Materia> proOptional = mateiraRepository.findById(id);

		return proOptional.orElseThrow(() -> new ObjectNotFoundException(
				"Objeto não encontrado! Id: " + id + ", Tipo: " + Materia.class.getName()));
	}

	public Materia insertMateria(Materia materia) {
		materia.setId(null);

		return mateiraRepository.save(materia);
	}

	public Materia fromDTO(MateriaDTO materiaDTO) {
		return new Materia(null, materiaDTO.getNome());
	}

	public Materia updateMateria(Materia materia) {
		if (mateiraRepository.findById(materia.getId()) == null) {
			throw new ObjectNotFoundException("Não existe essa materia");

		}

		if (mateiraRepository.findByNome(materia.getNome()) != null) {
			throw new ObjectNotFoundException("Materia já existe");
		}

		return mateiraRepository.save(materia);

	}

	public void deleteMateria(Integer id) {
		if (mateiraRepository.findById(id) == null) {
			throw new ObjectNotFoundException("Não existe essa materia");

		}
		mateiraRepository.deleteById(id);
	}
}
