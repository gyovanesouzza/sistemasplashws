package com.gyovanesouzza.springboost.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gyovanesouzza.springboost.Repository.AtividadeRepository;
import com.gyovanesouzza.springboost.beans.Atividade;
import com.gyovanesouzza.springboost.exceptions.ObjectNotFoundException;

@Service
public class AtividadeService {

	@Autowired
	private AtividadeRepository alunoRepository;

	public List<Atividade> findAll() {
		return alunoRepository.findAll();
	}

	public Atividade findById(Integer id) {

		Optional<Atividade> aluno = alunoRepository.findById(id);

		return aluno.orElseThrow(() -> new ObjectNotFoundException(
				"Objeto não encontrado! Id: " + id + ", Tipo: " + getClass().getName()));
	}
}
