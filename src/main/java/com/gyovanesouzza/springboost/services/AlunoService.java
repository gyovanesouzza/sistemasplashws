package com.gyovanesouzza.springboost.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gyovanesouzza.springboost.Repository.AlunoRepository;
import com.gyovanesouzza.springboost.Repository.EnderecoRepository;
import com.gyovanesouzza.springboost.Repository.TelefoneRepository;
import com.gyovanesouzza.springboost.Repository.UsuarioRepository;
import com.gyovanesouzza.springboost.beans.Aluno;
import com.gyovanesouzza.springboost.beans.Endereco;
import com.gyovanesouzza.springboost.beans.Telefone;
import com.gyovanesouzza.springboost.beans.Usuario;
import com.gyovanesouzza.springboost.beans.dto.AlunoDTO;
import com.gyovanesouzza.springboost.beans.dto.AlunoUpdateDTO;
import com.gyovanesouzza.springboost.exceptions.ObjectNotFoundException;

@Service
public class AlunoService {

	@Autowired
	private AlunoRepository alunoRepository;
	@Autowired
	private UsuarioService usuarioService;
	@Autowired
	private UsuarioRepository usuarioRepository;
	@Autowired
	private TelefoneRepository telefoneRepository;
	@Autowired
	private EnderecoRepository enderecoRepository;

	public List<Aluno> findAll() {

		return alunoRepository.findAll();
	}

	public Aluno findById(Integer id) {

		Optional<Aluno> aluno = alunoRepository.findById(id);

		return aluno.orElseThrow(() -> new ObjectNotFoundException(
				"Objeto não encontrado! Id: " + id + ", Tipo: " + getClass().getName()));
	}

	public List<Aluno> findByEnderecoAndId(Aluno aluno) {
		aluno = findById(aluno.getRm());

		return alunoRepository.findByEnderecoAndRmNot(aluno.getEndereco(), aluno.getRm());
	}

	public Aluno insertAluno(Aluno aluno) {
		aluno.setRm(null);
		telefoneRepository.save(aluno.getTelefone());
		enderecoRepository.save(aluno.getEndereco());
		aluno = alunoRepository.save(aluno);

		return aluno;
	}

	public Aluno updateAluno(Aluno aluno) {

		Aluno newAluno = findById(aluno.getRm());

		updateData(newAluno, aluno);

		return alunoRepository.save(newAluno);
	}

	public Aluno fromNewDTO(AlunoDTO alunoDTO) {
		Endereco endereco = null;
		Telefone telefone = null;

		Aluno aluno = new Aluno(null, alunoDTO.getNome(), alunoDTO.getDt_nascimento(), alunoDTO.getMae(),
				alunoDTO.getPai(), alunoDTO.getCpf(), alunoDTO.getRg(), alunoDTO.getIdade(), null, null);

		endereco = enderecoRepository.findByEstadoAndCidadeAndCepAndBairroAndRuaAndNumeroAndComplemento(
				alunoDTO.getEstado(), alunoDTO.getCidade(), alunoDTO.getCep(), alunoDTO.getBairro(), alunoDTO.getRua(),
				alunoDTO.getNumero(), alunoDTO.getComplemento());

		telefone = telefoneRepository.findByDddTelefoneAndTelefoneAndDddCelularAndCelular(alunoDTO.getDddTelefone(),
				alunoDTO.getTelefone(), alunoDTO.getDddCelular(), alunoDTO.getCelular());

		if (endereco == null) {
			endereco = new Endereco(alunoDTO.getRua(), alunoDTO.getNumero(), alunoDTO.getBairro(), alunoDTO.getCep(),
					alunoDTO.getComplemento(), alunoDTO.getCidade(), alunoDTO.getEstado());
		}

		if (telefone == null) {
			telefone = new Telefone(null, alunoDTO.getDddTelefone(), alunoDTO.getDddCelular(), alunoDTO.getTelefone(),
					alunoDTO.getCelular());
		}
		aluno.setEndereco(endereco);
		aluno.setTelefone(telefone);

		Usuario usuario = usuarioService.fromNewAlunoDTO(alunoRepository.findTopByOrderByRmDesc(), alunoDTO);
		usuarioRepository.save(usuario);

		aluno.setUsuario(usuario);
		return aluno;

	}

	public Aluno fromUpdateDTO(AlunoUpdateDTO alunoUpdateDTO) {

		Aluno aluno = new Aluno(null, alunoUpdateDTO.getNome(), alunoUpdateDTO.getDt_nascimento(), null, null, null,
				null, null, null, null);
		aluno.setFoto(alunoUpdateDTO.getFoto());

		Endereco endereco = new Endereco(alunoUpdateDTO.getRua(), alunoUpdateDTO.getNumero(),
				alunoUpdateDTO.getBairro(), alunoUpdateDTO.getCep(), alunoUpdateDTO.getComplemento(),
				alunoUpdateDTO.getCidade(), alunoUpdateDTO.getEstado());

		Telefone tel1 = new Telefone(null, alunoUpdateDTO.getDddTelefone(), alunoUpdateDTO.getDddCelular(),
				alunoUpdateDTO.getTelefone(), alunoUpdateDTO.getCelular());

		aluno.setEndereco(endereco);
		aluno.setTelefone(tel1);

		return aluno;

	}

	private void updateData(Aluno newAluno, Aluno aluno) {

		newAluno.setNome(aluno.getNome());
		newAluno.setDt_nascimento(aluno.getDt_nascimento());
		newAluno.setFoto(aluno.getFoto());

		Endereco endereco = newAluno.getEndereco();

		if (!aluno.getEndereco().getEstado().equals(newAluno.getEndereco().getEstado())
				&& aluno.getEndereco().getEstado() != null) {
			endereco.setEstado(aluno.getEndereco().getEstado());
		}
		if (!aluno.getEndereco().getCidade().equals(newAluno.getEndereco().getCidade())
				&& aluno.getEndereco().getCidade() != null) {
			endereco.setCidade(aluno.getEndereco().getCidade());
		}
		if (!aluno.getEndereco().getCep().equals(newAluno.getEndereco().getCep())
				&& aluno.getEndereco().getCep() != null) {
			endereco.setCep(aluno.getEndereco().getCep());
		}
		if (!aluno.getEndereco().getBairro().equals(newAluno.getEndereco().getBairro())
				&& aluno.getEndereco().getBairro() != null) {
			endereco.setBairro(aluno.getEndereco().getBairro());
		}
		if (!aluno.getEndereco().getRua().equals(newAluno.getEndereco().getRua())
				&& aluno.getEndereco().getRua() != null) {
			endereco.setRua(aluno.getEndereco().getRua());
		}
		if (!aluno.getEndereco().getNumero().equals(newAluno.getEndereco().getNumero())
				&& aluno.getEndereco().getNumero() != null) {
			endereco.setNumero(aluno.getEndereco().getNumero());
		}
		if (!aluno.getEndereco().getComplemento().equals(newAluno.getEndereco().getComplemento())) {
			endereco.setComplemento(aluno.getEndereco().getComplemento());
		}
		newAluno.setEndereco(endereco);

		Telefone telefone = newAluno.getTelefone();

		if (!aluno.getTelefone().getDddTelefone().equals(newAluno.getTelefone().getDddTelefone())
				&& aluno.getTelefone().getDddTelefone() != null) {
			telefone.setDddTelefone(aluno.getTelefone().getDddTelefone());
		}
		if (!aluno.getTelefone().getTelefone().equals(newAluno.getTelefone().getTelefone())
				&& aluno.getTelefone().getTelefone() != null) {
			telefone.setTelefone(aluno.getTelefone().getTelefone());

		}
		if (!aluno.getTelefone().getDddCelular().equals(newAluno.getTelefone().getDddCelular())
				&& aluno.getTelefone().getDddCelular() != null) {
			telefone.setDddCelular(aluno.getTelefone().getDddCelular());

		}
		if (!aluno.getTelefone().getCelular().equals(newAluno.getTelefone().getCelular())
				&& aluno.getTelefone().getCelular() != null) {
			telefone.setCelular(aluno.getTelefone().getCelular());

		}
		newAluno.setTelefone(telefone);

	}

}
