package com.gyovanesouzza.springboost.services;

import java.sql.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.gyovanesouzza.springboost.Repository.TurmaRepository;
import com.gyovanesouzza.springboost.beans.Turma;
import com.gyovanesouzza.springboost.beans.dto.TurmaDTO;
import com.gyovanesouzza.springboost.exceptions.DataIntegrityException;
import com.gyovanesouzza.springboost.exceptions.ObjectNotFoundException;

@Service
public class TurmaService {

	@Autowired
	private TurmaRepository turmaRepository;

	public List<Turma> findAll() {
		return turmaRepository.findAll();
	}

	public Turma findById(Integer id) {
		Optional<Turma> proOptional = turmaRepository.findById(id);

		return proOptional.orElseThrow(() -> new ObjectNotFoundException(
				"Objeto não encontrado! Id: " + id + ", Tipo: " + Turma.class.getName()));
	}

	public Turma existTurmaByAtribut(Turma turma) {

		Turma obj = turmaRepository.findByTurmaAndAnoSerieAndAnoTurmaAndIdNot(turma.getTurma(), turma.getAnoSerie(),
				new Date(turma.getAnoTurma().getTime()), turma.getId());

		return obj;
	}

	@Transactional
	public Turma insert(Turma turma) {
		turma.setId(null);

		Turma obj = turmaRepository.save(turma);
		return obj;
	}

	public Turma update(Turma turma) {
		Turma turmanew = findById(turma.getId());
		updateNewTurma(turmanew, turma);
		Turma existe = existTurmaByAtribut(turmanew);
		if (existe != null) {
			throw new ObjectNotFoundException("Turma ja cadastrada");
		}
		return turmaRepository.save(turmanew);
	}

	private void updateNewTurma(Turma turmanew, Turma turma) {
		turmanew.setTurma(turma.getTurma());
		turmanew.setAnoSerie(turma.getAnoSerie());
	}

	public Turma fromDTO(TurmaDTO turmaDTO) {

		Turma turma = new Turma(null, turmaDTO.getAno_serie(), turmaDTO.getTurma(), turmaDTO.getAno_turma());

		return turma;
	}

	public void delete(Integer id) {
		if (findById(id) != null) {

			try {
				turmaRepository.deleteById(id);
			} catch (DataIntegrityViolationException e) {
				throw new DataIntegrityException("Não é possivel excluir uma turma que possui Alunos");
			}
		} else {
			throw new DataIntegrityException("Não é possivel excluir uma turma que não existe");
		}
	}
}
