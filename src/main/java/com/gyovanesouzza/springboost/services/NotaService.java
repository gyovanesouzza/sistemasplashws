package com.gyovanesouzza.springboost.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gyovanesouzza.springboost.Repository.NotaRepository;
import com.gyovanesouzza.springboost.beans.Nota;
import com.gyovanesouzza.springboost.exceptions.ObjectNotFoundException;

@Service
public class NotaService {
	@Autowired
	private NotaRepository notaRepository;

	public List<Nota> findAll() {
		return notaRepository.findAll();
	}

	public Nota findById(Integer id) {

		Optional<Nota> proOptional = notaRepository.findById(id);

		return proOptional.orElseThrow(() -> new ObjectNotFoundException(
				"Objeto não encontrado! Id: " + id + ", Tipo: " + Nota.class.getName()));
	}
}
