package com.gyovanesouzza.springboost.services;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gyovanesouzza.springboost.Repository.UsuarioRepository;
import com.gyovanesouzza.springboost.beans.Aluno;
import com.gyovanesouzza.springboost.beans.Professor;
import com.gyovanesouzza.springboost.beans.Usuario;
import com.gyovanesouzza.springboost.beans.dto.AlunoDTO;
import com.gyovanesouzza.springboost.beans.dto.ProfessorDTO;
import com.gyovanesouzza.springboost.exceptions.ObjectNotFoundException;

@Service
public class UsuarioService {

	@Autowired
	private UsuarioRepository usuarioRepository;

	private static final String ZERO_FILL = "%06d";

	public List<Usuario> findAll() {
		return usuarioRepository.findAll();
	}

	public Usuario findById(Integer id) {
		Optional<Usuario> proOptional = usuarioRepository.findById(id);
		return proOptional.orElseThrow(() -> new ObjectNotFoundException(
				"Objeto não encontrado! Id: " + id + ", Tipo: " + Usuario.class.getName()));
	}

	public Usuario findByUsernameAndSenha(Usuario usuario) {

		Usuario obj = usuarioRepository.findByUsernameAndSenha(usuario.getUsername(), usuario.getSenha());

		if (obj == null) {
			throw new ObjectNotFoundException("Usuario não encontrado");
		}
		return obj;
	}

	public Usuario fromNewAlunoDTO(Aluno aluno, AlunoDTO alunoDTO) {

		Usuario usuario = new Usuario(null,
				(aluno == null ? String.format(ZERO_FILL, 1) : String.format(ZERO_FILL, aluno.getRm() + 1)),
				createSenha(alunoDTO.getDt_nascimento()), alunoDTO.getEmail(), alunoDTO.getNome());
		return usuario;
	}

	public Usuario fromProfessorDTO(Professor professor, ProfessorDTO professorDTO) {

		Usuario usuario = new Usuario(null,
				(professor == null ? "PR" + String.format(ZERO_FILL, 1)
						: "PR" +String.format(ZERO_FILL, professor.getId() + 1)),
				createSenha(professorDTO.getDt_nascimento()), professorDTO.getEmail(), professorDTO.getNome());

		return usuario;
	}

	private String createSenha(Date dt_nascimento) {

		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yy");
		String senha = dateFormat.format(dt_nascimento).replace("//", "//").replace("//", "//").trim();

		return senha;
	}
}
