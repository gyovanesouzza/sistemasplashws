package com.gyovanesouzza.springboost.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gyovanesouzza.springboost.Repository.EnderecoRepository;
import com.gyovanesouzza.springboost.Repository.ProfessorRepository;
import com.gyovanesouzza.springboost.Repository.TelefoneRepository;
import com.gyovanesouzza.springboost.Repository.UsuarioRepository;
import com.gyovanesouzza.springboost.beans.Endereco;
import com.gyovanesouzza.springboost.beans.Professor;
import com.gyovanesouzza.springboost.beans.Telefone;
import com.gyovanesouzza.springboost.beans.Usuario;
import com.gyovanesouzza.springboost.beans.dto.ProfessorDTO;
import com.gyovanesouzza.springboost.exceptions.ObjectNotFoundException;

@Service
public class ProfessorService {

	@Autowired
	private ProfessorRepository professorRepository;
	@Autowired
	private UsuarioService usuarioService;
	@Autowired
	private UsuarioRepository usuarioRepository;
	@Autowired
	private TelefoneRepository telefoneRepository;
	@Autowired
	private EnderecoRepository enderecoRepository;

	public List<Professor> findAll() {
		return professorRepository.findAll();
	}

	public Professor findById(Integer id) {

		Optional<Professor> proOptional = professorRepository.findById(id);

		return proOptional.orElseThrow(() -> new ObjectNotFoundException(
				"Objeto não encontrado! Id: " + id + ", Tipo: " + Professor.class.getName()));
	}

	public Professor insertProfessor(Professor professor) {

		professor.setId(null);
		telefoneRepository.save(professor.getTelefone());
		enderecoRepository.save(professor.getEndereco());
		professor = professorRepository.save(professor);

		return professor;
	}

	public Professor updateProfessor(Professor professor) {

		Professor newprofessor = findById(professor.getId());
		updateDataProfessor(newprofessor, professor);

		newprofessor = professorRepository.save(newprofessor);

		return newprofessor;
	}

	private void updateDataProfessor(Professor newprofessor, Professor professor) {

		newprofessor.setNome(professor.getNome());
		newprofessor.setDt_nascimento((professor.getDt_nascimento() == null) ? newprofessor.getDt_nascimento()
				: professor.getDt_nascimento());

		Endereco endereco = newprofessor.getEndereco();

		if (professor.getEndereco().getEstado() != null
				&& !professor.getEndereco().getEstado().equals(newprofessor.getEndereco().getEstado())) {
			endereco.setEstado(professor.getEndereco().getEstado());
		}
		if (professor.getEndereco().getCidade() != null
				&& !professor.getEndereco().getCidade().equals(newprofessor.getEndereco().getCidade())) {

			endereco.setCidade(professor.getEndereco().getCidade());
		}
		if (professor.getEndereco().getCep() != null
				&& !professor.getEndereco().getCep().equals(newprofessor.getEndereco().getCep())) {
			endereco.setCep(professor.getEndereco().getCep());
		}
		if (professor.getEndereco().getBairro() != null
				&& !professor.getEndereco().getBairro().equals(newprofessor.getEndereco().getBairro())) {
			endereco.setBairro(professor.getEndereco().getBairro());
		}
		if (professor.getEndereco().getRua() != null
				&& !professor.getEndereco().getRua().equals(newprofessor.getEndereco().getRua())) {
			endereco.setRua(professor.getEndereco().getRua());
		}
		if (professor.getEndereco().getNumero() != null
				&& !professor.getEndereco().getNumero().equals(newprofessor.getEndereco().getNumero())) {
			endereco.setNumero(professor.getEndereco().getNumero());
		}

		endereco.setComplemento(
				(professor.getEndereco().getComplemento() != newprofessor.getEndereco().getComplemento()) ? null
						: professor.getEndereco().getComplemento());

		newprofessor.setEndereco(endereco);

		Telefone telefone = newprofessor.getTelefone();

		if (professor.getTelefone().getDddTelefone() != null
				&& !professor.getTelefone().getDddTelefone().equals(newprofessor.getTelefone().getDddTelefone())) {
			telefone.setDddTelefone(professor.getTelefone().getDddTelefone());
		}
		if (professor.getTelefone().getTelefone() != null
				&& !professor.getTelefone().getTelefone().equals(newprofessor.getTelefone().getTelefone())) {
			telefone.setTelefone(professor.getTelefone().getTelefone());

		}
		if (professor.getTelefone().getDddCelular() != null
				&& !professor.getTelefone().getDddCelular().equals(newprofessor.getTelefone().getDddCelular())) {
			telefone.setDddCelular(professor.getTelefone().getDddCelular());

		}
		if (professor.getTelefone().getCelular() != null
				&& !professor.getTelefone().getCelular().equals(newprofessor.getTelefone().getCelular())) {
			telefone.setCelular(professor.getTelefone().getCelular());

		}
		newprofessor.setTelefone(telefone);

	}

	public Professor fromNewDTO(ProfessorDTO professorDTO) {
		Endereco endereco = null;
		Telefone telefone = null;

		Professor professor = new Professor(null, professorDTO.getNome(), professorDTO.getDt_nascimento(),
				professorDTO.getCpf(), professorDTO.getRg(), null);

		endereco = enderecoRepository.findByEstadoAndCidadeAndCepAndBairroAndRuaAndNumeroAndComplemento(
				professorDTO.getEstado(), professorDTO.getCidade(), professorDTO.getCep(), professorDTO.getBairro(),
				professorDTO.getRua(), professorDTO.getNumero(), professorDTO.getComplemento());

		telefone = telefoneRepository.findByDddTelefoneAndTelefoneAndDddCelularAndCelular(professorDTO.getDddTelefone(),
				professorDTO.getTelefone(), professorDTO.getDddCelular(), professorDTO.getCelular());

		if (endereco == null) {
			endereco = new Endereco(professorDTO.getRua(), professorDTO.getNumero(), professorDTO.getBairro(),
					professorDTO.getCep(), professorDTO.getComplemento(), professorDTO.getCidade(),
					professorDTO.getEstado());
		}

		if (telefone == null) {
			telefone = new Telefone(null, professorDTO.getDddTelefone(), professorDTO.getDddCelular(),
					professorDTO.getTelefone(), professorDTO.getCelular());
		}
		professor.setEndereco(endereco);
		professor.setTelefone(telefone);

		Usuario usuario = usuarioService.fromProfessorDTO(professorRepository.findTopByOrderByIdDesc(), professorDTO);
		usuarioRepository.save(usuario);
		professor.setUsuario(usuario);
		return professor;
	}

	public Professor fromUpdateDTO(ProfessorDTO professorDTO) {

		Professor professor = new Professor(null, professorDTO.getNome(), professorDTO.getDt_nascimento(),
				professorDTO.getCpf(), professorDTO.getRg(), null);

		Endereco endereco = new Endereco(professorDTO.getRua(), professorDTO.getNumero(), professorDTO.getBairro(),
				professorDTO.getCep(), professorDTO.getComplemento(), professorDTO.getCidade(),
				professorDTO.getEstado());

		Telefone telefone = new Telefone(null, professorDTO.getDddTelefone(), professorDTO.getDddCelular(),
				professorDTO.getTelefone(), professorDTO.getCelular());

		professor.setEndereco(endereco);
		professor.setTelefone(telefone);

		return professor;
	}

}
