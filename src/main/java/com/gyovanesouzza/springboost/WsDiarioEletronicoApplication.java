package com.gyovanesouzza.springboost;

import java.util.Arrays;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.gyovanesouzza.springboost.Repository.AlunoRepository;
import com.gyovanesouzza.springboost.Repository.AtividadeRepository;
import com.gyovanesouzza.springboost.Repository.EnderecoRepository;
import com.gyovanesouzza.springboost.Repository.FaltaRepository;
import com.gyovanesouzza.springboost.Repository.MateriaRepository;
import com.gyovanesouzza.springboost.Repository.NotaRepository;
import com.gyovanesouzza.springboost.Repository.ProfessorRepository;
import com.gyovanesouzza.springboost.Repository.TelefoneRepository;
import com.gyovanesouzza.springboost.Repository.TurmaRepository;
import com.gyovanesouzza.springboost.Repository.UsuarioRepository;
import com.gyovanesouzza.springboost.beans.Aluno;
import com.gyovanesouzza.springboost.beans.Atividade;
import com.gyovanesouzza.springboost.beans.Endereco;
import com.gyovanesouzza.springboost.beans.Materia;
import com.gyovanesouzza.springboost.beans.Nota;
import com.gyovanesouzza.springboost.beans.Professor;
import com.gyovanesouzza.springboost.beans.Telefone;
import com.gyovanesouzza.springboost.beans.Turma;
import com.gyovanesouzza.springboost.beans.Usuario;

@SpringBootApplication
public class WsDiarioEletronicoApplication implements CommandLineRunner {

	@Autowired
	private AlunoRepository alunoRepository;
	@Autowired
	private ProfessorRepository professorRepository;
	@Autowired
	private EnderecoRepository enderecoRepository;
	@Autowired
	private TurmaRepository turmaRepository;
	@Autowired
	private UsuarioRepository usuarioRepository;
	@Autowired
	private FaltaRepository faltaRepository;
	@Autowired
	private MateriaRepository materiaRepository;

	@Autowired
	private AtividadeRepository atividadeRepository;
	@Autowired
	private NotaRepository notaRepository;
	@Autowired
	private TelefoneRepository telefoneRepository;

	public static void main(String[] args) {
		SpringApplication.run(WsDiarioEletronicoApplication.class, args);

	}

	@Override
	public void run(String... args) throws Exception {

		Usuario user1 = new Usuario(null, "aluno1", "Rot", "roo@outlook.com", "Gyovane Aluno");
		Usuario user2 = new Usuario(null, "aluno2", "oot", "root@otlook.com", "Giovanni Aluno");
		Usuario user3 = new Usuario(null, "aluno3", "Roo", "@otlok.com", "Maria Aluno");
		Usuario user4 = new Usuario(null, "Professor1", "Ro", "r@otlook.com", "Rennan Professor");
		Usuario user5 = new Usuario(null, "Professor2", "Rt", "root@ouook.com", "Jose Professor");

		usuarioRepository.saveAll(Arrays.asList(user1, user2, user3, user4, user5));

		Endereco e1 = new Endereco("Eu aqui", "52", "são luis", "0494311-1", null, "São Miguel", "SP");
		Endereco e2 = new Endereco("Ra", "f", "FF", "09494949", "4554", "sao", "SP");

		enderecoRepository.saveAll(Arrays.asList(e1, e2));

		Telefone t1 = new Telefone(null, "11", "11", "58331746", "11165");
		Telefone t2 = new Telefone(null, "121", null, "111", null);
		
		Professor p1 = new Professor(null, "Rennan", new Date(), "1515415", "15151514", user4);
		Professor p2 = new Professor(null, "Jose", new Date(), "15151414", "15151511", user5);
		p1.setEndereco(e1);
		p1.setTelefone(t2);
		p2.setEndereco(e1);
		Turma tu1 = new Turma(null, "1", "A", new Date());
		Turma tu2 = new Turma(null, "1	", "B", new Date());
		Turma tu3 = new Turma(null, "1", "C", new Date());

		tu1.getProfessores().addAll(Arrays.asList(p1, p2));
		tu2.getProfessores().addAll(Arrays.asList(p2));
		tu3.getProfessores().addAll(Arrays.asList(p1));

		p1.getTurmas().addAll(Arrays.asList(tu1, tu3));
		p2.getTurmas().addAll(Arrays.asList(tu1, tu2));
		telefoneRepository.saveAll(Arrays.asList(t1, t2));

		turmaRepository.saveAll(Arrays.asList(tu1, tu2, tu3));
		professorRepository.saveAll(Arrays.asList(p1, p2));

		Aluno al1 = new Aluno(null, "Gyovane", new Date(), "Giomar", "Gilson", "2511515", "rgh", 17, "URL", user1);
		Aluno al2 = new Aluno(null, "Giovanni", new Date(), "Giomar", "Gilson", "351515", "hfrg", 16, "URL", user2);

		Aluno al3 = new Aluno(null, "Maria", new Date(), "Giomar", "Gilson", "3315", "frg", 15, "URL", user3);


		al1.setTelefone(t1);
		al2.setTelefone(t2);
		al3.setTelefone(t2);

		al2.setEndereco(e2);
		tu1.getAlunos().addAll(Arrays.asList(al1, al2));
		tu2.getAlunos().addAll(Arrays.asList(al3));

		Materia m1 = new Materia(null, "Portugues");
		Materia m2 = new Materia(null, "Matematica");
		Materia m3 = new Materia(null, "Historia");
		Materia m4 = new Materia(null, "Geografia");
		Materia m5 = new Materia(null, "Ingles");
		Materia m6 = new Materia(null, "Fisica");
		Materia m7 = new Materia(null, "Quimica");

		m1.getProfessores().addAll(Arrays.asList(p1, p2));
		m2.getProfessores().addAll(Arrays.asList(p1));
		m3.getProfessores().addAll(Arrays.asList(p2));
		m4.getProfessores().addAll(Arrays.asList(p1, p2));
		m5.getProfessores().addAll(Arrays.asList(p2));
		m6.getProfessores().addAll(Arrays.asList(p1));
		m7.getProfessores().addAll(Arrays.asList(p1, p2));

		p1.getMaterias().addAll(Arrays.asList(m1, m2, m4, m6, m7));
		p2.getMaterias().addAll(Arrays.asList(m3, m4, m5, m7));

		Atividade at1 = new Atividade(null, "Caderno", "Ex 15 e 16", new Date(), 5.0, m1, p1);
		Atividade at2 = new Atividade(null, "Caderno", "Ex 17 e 19", new Date(), 5.2, m2, p1);
		Atividade at3 = new Atividade(null, "Livro", "pag 4 e 5", new Date(), 1.5, m3, p2);
		Atividade at4 = new Atividade(null, "Livro", "pag 15 e 16", new Date(), 0.5, m5, p2);

//		al1.getAtividades().addAll(Arrays.asList(at1, at2, at3, at4));
//		al2.getAtividades().addAll(Arrays.asList(at1, at2));
//		al3.getAtividades().addAll(Arrays.asList(at3, at4));

		m1.getAtividades().addAll(Arrays.asList(at1));
		m2.getAtividades().addAll(Arrays.asList(at2));
		m5.getAtividades().addAll(Arrays.asList(at4));

//		at1.getAlunos().addAll(Arrays.asList(al1, al2));
//		at2.getAlunos().addAll(Arrays.asList(al1, al2));
//		at3.getAlunos().addAll(Arrays.asList(al1, al3));
//		at4.getAlunos().addAll(Arrays.asList(al1, al3));

		Nota nt1 = new Nota(10.0, al1, at1);
		Nota nt2 = new Nota(5.0, al2, at1);
		Nota nt3 = new Nota(3.0, al3, at1);
		Nota nt4 = new Nota(8.0, al1, at2);

		materiaRepository.saveAll(Arrays.asList(m1, m2, m3, m4, m5, m7));
		atividadeRepository.saveAll(Arrays.asList(at1, at2, at3, at4));
		alunoRepository.saveAll(Arrays.asList(al1, al2, al3));
		notaRepository.saveAll(Arrays.asList(nt1, nt2, nt3, nt4));
	}
//	Falta f1 = new Falta(new Timestamp(System.currentTimeMillis()), al1, p1);
//	Falta f2 = new Falta(new Timestamp(System.currentTimeMillis()), al1, p1);
//	Falta f3 = new Falta(new Timestamp(System.currentTimeMillis()), al1, p1);
//	Falta f4 = new Falta(new Timestamp(System.currentTimeMillis()), al3, p1);
//	Falta f5 = new Falta(new Timestamp(System.currentTimeMillis()), al2, p1);
//
//	al1.getFaltas().addAll(Arrays.asList(f1, f2, f3, f4));
//	al2.getFaltas().addAll(Arrays.asList(f5));
//	
//	p1.getFaltas().addAll(Arrays.asList(f1, f2, f3, f4,f5));
//	
//
//	faltaRepository.saveAll(Arrays.asList(f1, f2, f3, f4, f5));
}
