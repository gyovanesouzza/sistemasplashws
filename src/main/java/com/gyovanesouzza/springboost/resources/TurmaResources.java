package com.gyovanesouzza.springboost.resources;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.gyovanesouzza.springboost.beans.Turma;
import com.gyovanesouzza.springboost.beans.dto.TurmaDTO;
import com.gyovanesouzza.springboost.services.TurmaService;

@RestController
@RequestMapping("/turmas")
public class TurmaResources {

	@Autowired
	private TurmaService turmaService;

	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<Turma>> findAll() {

		List<Turma> list = turmaService.findAll();
		// List<TurmaDTO> listDto = list.stream().map(obj -> new
		// TurmaDTO(obj)).collect(Collectors.toList());
		return ResponseEntity.ok().body(list);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{id}")
	public ResponseEntity<Turma> findByID(@PathVariable Integer id) {

		Turma turma = turmaService.findById(id);

		return ResponseEntity.ok().body(turma);
	}

	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Void> insertTurma(@RequestBody TurmaDTO turmaDTO) {

		Turma turma = turmaService.fromDTO(turmaDTO);
		turma = turmaService.insert(turma);
		URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(turma.getId()).toUri();

		return ResponseEntity.created(uri).build();
	}

	@RequestMapping(method = RequestMethod.PUT, value = "/{id}")
	public ResponseEntity<Void> updateTurma(@RequestBody TurmaDTO turmaDTO, @PathVariable Integer id) {

		Turma turma = turmaService.fromDTO(turmaDTO);
		turma.setId(id);
		turma = turmaService.update(turma);

		return ResponseEntity.noContent().build();
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Void> deleteTurma(@PathVariable Integer id) {

		turmaService.delete(id);
		return ResponseEntity.noContent().build();

	}

}
