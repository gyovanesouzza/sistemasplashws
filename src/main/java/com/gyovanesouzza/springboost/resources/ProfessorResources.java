package com.gyovanesouzza.springboost.resources;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.gyovanesouzza.springboost.beans.Professor;
import com.gyovanesouzza.springboost.beans.dto.ProfessorDTO;
import com.gyovanesouzza.springboost.services.ProfessorService;

@RestController
@RequestMapping("/professores")
public class ProfessorResources {

	@Autowired
	private ProfessorService professorService;

	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<Professor>> findAll() {

		List<Professor> list = professorService.findAll();

		return ResponseEntity.ok().body(list);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{id}")
	public ResponseEntity<Professor> findByID(@PathVariable Integer id) {

		Professor professor = professorService.findById(id);

		return ResponseEntity.ok().body(professor);
	}

	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Void> insertProfessor(@RequestBody ProfessorDTO professorDTO) {

		Professor professor = professorService.fromNewDTO(professorDTO);
		professor = professorService.insertProfessor(professor);

		URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(professor.getId())
				.toUri();

		return ResponseEntity.created(uri).build();
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Void> UpdateProfessor(@Valid @RequestBody ProfessorDTO professorDTO, @PathVariable Integer id) {

		Professor professor = professorService.fromUpdateDTO(professorDTO);
		professor.setId(id);
		professor = professorService.updateProfessor(professor);

		return ResponseEntity.noContent().build();
	}

}
