package com.gyovanesouzza.springboost.resources;

import java.net.URI;
import java.util.List;

import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.gyovanesouzza.springboost.beans.Aluno;
import com.gyovanesouzza.springboost.beans.dto.AlunoDTO;
import com.gyovanesouzza.springboost.beans.dto.AlunoUpdateDTO;
import com.gyovanesouzza.springboost.services.AlunoService;

@RestController
@RequestMapping("/alunos")
public class AlunoResources {

	@Autowired
	private AlunoService alunoService;

	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<Aluno>> findAll() {

		List<Aluno> list = alunoService.findAll();
		return ResponseEntity.ok().body(list);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<Aluno> findById(@PathVariable Integer id) {
		Aluno aluno = alunoService.findById(id);
		return ResponseEntity.ok().body(aluno);
	}

	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Void> insertAluno(@RequestBody AlunoDTO alunoDTO) {

		Aluno aluno = alunoService.fromNewDTO(alunoDTO);
		aluno = alunoService.insertAluno(aluno);

		URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(aluno.getRm()).toUri();

		return ResponseEntity.created(uri).build();
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Void> updateAluno(@RequestBody AlunoUpdateDTO alunoUpdateDTO, @PathParam("id") Integer id) {

		Aluno aluno = alunoService.fromUpdateDTO(alunoUpdateDTO);
		aluno = alunoService.updateAluno(aluno);

		return ResponseEntity.noContent().build();
	}

	
	@RequestMapping(value = "/test/{id}", method = RequestMethod.GET)
	public ResponseEntity<List<Aluno>> findTest(@PathVariable Integer id) {

		Aluno aluno = new Aluno();
		aluno.setRm(id);
		List<Aluno> list = alunoService.findByEnderecoAndId(aluno);
		return ResponseEntity.ok().body(list);
	}
	
}
