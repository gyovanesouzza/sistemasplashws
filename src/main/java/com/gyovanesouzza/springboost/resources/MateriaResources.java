package com.gyovanesouzza.springboost.resources;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.gyovanesouzza.springboost.beans.Materia;
import com.gyovanesouzza.springboost.beans.dto.MateriaDTO;
import com.gyovanesouzza.springboost.services.MateriaService;

@RestController
@RequestMapping("/materias")
public class MateriaResources {

	@Autowired
	private MateriaService materiaService;

	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<Materia>> findAll() {

		List<Materia> list = materiaService.findAll();
		// List<MateriaDTO> listDto = list.stream().map(obj -> new
		// MateriaDTO(obj)).collect(Collectors.toList());
		return ResponseEntity.ok().body(list);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{id}")
	public ResponseEntity<Materia> findByID(@PathVariable Integer id) {

		Materia materia = materiaService.findById(id);

		return ResponseEntity.ok().body(materia);
	}

	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Void> insertMateria(@RequestBody MateriaDTO materiaDTO) {

		Materia materia = materiaService.fromDTO(materiaDTO);
		materia = materiaService.insertMateria(materia);

		URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(materia.getId())
				.toUri();

		return ResponseEntity.created(uri).build();

	}

	@RequestMapping(method = RequestMethod.PUT, value = "/{id}")
	public ResponseEntity<Void> updateMateria(@RequestBody MateriaDTO materiaDTO, @PathVariable Integer id) {

		Materia materia = materiaService.fromDTO(materiaDTO);
		materia.setId(id);
		materia = materiaService.updateMateria(materia);

		return ResponseEntity.noContent().build();

	}

	@RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
	public ResponseEntity<Void> deleteMateria( @PathVariable Integer id) {

	
		materiaService.deleteMateria(id);

		return ResponseEntity.noContent().build();

	}

}
