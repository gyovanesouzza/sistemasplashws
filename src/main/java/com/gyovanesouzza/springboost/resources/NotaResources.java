package com.gyovanesouzza.springboost.resources;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.gyovanesouzza.springboost.beans.Nota;
import com.gyovanesouzza.springboost.services.NotaService;

@RestController
@RequestMapping("/notas")
public class NotaResources {

	@Autowired
	private NotaService notaService;

	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<Nota>> findAll() {

		List<Nota> list = notaService.findAll();
		// List<NotaDTO> listDto = list.stream().map(obj -> new
		// NotaDTO(obj)).collect(Collectors.toList());
		return ResponseEntity.ok().body(list);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{id}")
	public ResponseEntity<Nota> findByID(@PathVariable Integer id) {

		Nota nota = notaService.findById(id);

		return ResponseEntity.ok().body(nota);
	}

}
