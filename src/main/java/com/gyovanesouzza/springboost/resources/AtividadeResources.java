package com.gyovanesouzza.springboost.resources;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.gyovanesouzza.springboost.beans.Atividade;
import com.gyovanesouzza.springboost.services.AtividadeService;

@RestController
@RequestMapping("/atividades")
public class AtividadeResources {

	@Autowired
	private AtividadeService materiaService;

	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<Atividade>> findAll() {

		List<Atividade> list = materiaService.findAll();
		//List<AtividadeDTO> listDto = list.stream().map(obj -> new AtividadeDTO(obj)).collect(Collectors.toList());
		return ResponseEntity.ok().body(list);
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/{id}")
	public ResponseEntity<Atividade> findByID(@PathVariable Integer id){
		
		Atividade materia = materiaService.findById(id);
		
		return ResponseEntity.ok().body(materia);
	}
	
}
