package com.gyovanesouzza.springboost.beans.dto;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public class TurmaDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String ano_serie;
	private String turma;
	@JsonFormat(pattern = "yyyy")
	private Date ano_turma;

	public TurmaDTO() {
		// TODO Auto-generated constructor stub
	}

	public TurmaDTO(String ano_serie, String turma, Date ano_turma) {
		super();
		this.ano_serie = ano_serie;
		this.turma = turma;
		this.ano_turma = ano_turma;
	}

	public String getAno_serie() {
		return ano_serie;
	}

	public void setAno_serie(String ano_serie) {
		this.ano_serie = ano_serie;
	}

	public String getTurma() {
		return turma;
	}

	public void setTurma(String turma) {
		this.turma = turma;
	}

	public Date getAno_turma() {
		return ano_turma;
	}

	public void setAno_turma(Date ano_turma) {
		this.ano_turma = ano_turma;
	}

}
