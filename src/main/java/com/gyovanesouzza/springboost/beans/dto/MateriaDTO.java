package com.gyovanesouzza.springboost.beans.dto;

import java.io.Serializable;

public class MateriaDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	private String nome;

	public MateriaDTO() {
		// TODO Auto-generated constructor stub
	}

	public MateriaDTO(String nome) {
		super();
		this.nome = nome;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

}
