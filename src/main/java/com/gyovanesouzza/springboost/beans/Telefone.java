package com.gyovanesouzza.springboost.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
public class Telefone implements Serializable {

	private static final long serialVersionUID = 1L;
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	private String dddTelefone;
	private String dddCelular;
	private String telefone;
	private String celular;
	
	@JsonBackReference
	@OneToMany(mappedBy = "telefone", cascade = CascadeType.MERGE)
	private List<Aluno> alunos = new ArrayList<>();

	@JsonBackReference
	@OneToMany(mappedBy = "telefone", cascade = CascadeType.MERGE)
	private List<Professor> professores = new ArrayList<Professor>();

	public Telefone() {
	}

	
	public Telefone(Integer id, String dddTelefone, String dddCelular, String telefone, String celular) {
		super();
		this.id = id;
		this.dddTelefone = dddTelefone;
		this.dddCelular = dddCelular;
		this.telefone = telefone;
		this.celular = celular;
	}


	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public String getDddTelefone() {
		return dddTelefone;
	}


	public void setDddTelefone(String dddTelefone) {
		this.dddTelefone = dddTelefone;
	}


	public String getDddCelular() {
		return dddCelular;
	}


	public void setDddCelular(String dddCelular) {
		this.dddCelular = dddCelular;
	}


	public String getTelefone() {
		return telefone;
	}


	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}


	public String getCelular() {
		return celular;
	}


	public void setCelular(String celular) {
		this.celular = celular;
	}


	public List<Aluno> getAlunos() {
		return alunos;
	}


	public void setAlunos(List<Aluno> alunos) {
		this.alunos = alunos;
	}


	public List<Professor> getProfessores() {
		return professores;
	}


	public void setProfessores(List<Professor> professores) {
		this.professores = professores;
	}

}
