package com.gyovanesouzza.springboost.beans;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Nota {
	
	@JsonIgnore
	@EmbeddedId
	private NotaPK id = new NotaPK();
	private Double nota;

	public Nota() {
	}

	public Nota(Double nota, Aluno aluno, Atividade atividade) {
		super();
		this.id = new NotaPK(aluno, atividade);
		this.nota = nota;
	}

	public NotaPK getId() {
		return id;
	}

	public void setId(NotaPK id) {
		this.id = id;
	}

	public Double getNota() {
		return nota;
	}

	public void setNota(Double nota) {
		this.nota = nota;
	}

}
