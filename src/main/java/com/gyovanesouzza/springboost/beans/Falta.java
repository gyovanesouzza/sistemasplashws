package com.gyovanesouzza.springboost.beans;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Falta implements Serializable {

	private static final long serialVersionUID = 1L;
	@EmbeddedId
	private FaltasPK id = new FaltasPK();
	private Timestamp data;

	public Falta() {
	}

	public Falta(Timestamp data, Aluno aluno,  Professor professor) {
		super();
		this.data = data;
		this.id.setAluno(aluno);
		this.id.setProfessor(professor);
	}

	public Aluno getAluno() {
		return id.getAluno();
	}
	
	@JsonIgnore
	public Professor getProfessor() {
		return id.getProfessor();
	}

	

	public Timestamp getData() {
		return data;
	}

	public void setData(Timestamp data) {
		this.data = data;
	}

}
