package com.gyovanesouzza.springboost.beans;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
public class Atividade implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	private String tipo;
	private String descricao;
	private Date dia;
	private Double notaMax;

	@JsonManagedReference
	@ManyToOne
	@JoinColumn(name = "materia_id")
	private Materia materia;
	
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "professor_id")
	private Professor professor;

	@JsonBackReference
	@OneToMany(mappedBy = "id.atividade")
	private Set<Nota> nota = new HashSet<>();

	public Atividade() {
	}

	public Atividade(Integer id, String tipo, String descricao, Date dia, Double notaMax, Materia materia,
			Professor professor) {
		super();
		this.id = id;
		this.tipo = tipo;
		this.descricao = descricao;
		this.dia = dia;
		this.notaMax = notaMax;
		this.materia = materia;
		this.professor = professor;
	}

	public Integer getId() {
		return id;
	}

	public String getTipo() {
		return tipo;
	}

	public String getDescricao() {
		return descricao;
	}

	public Date getDia() {
		return dia;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public void setDia(Date dia) {
		this.dia = dia;
	}

	public Double getNotaMax() {
		return notaMax;
	}

	public void setNotaMax(Double notaMax) {
		this.notaMax = notaMax;
	}

	public Materia getMateria() {
		return materia;
	}

	public void setMateria(Materia materia) {
		this.materia = materia;
	}

	public Professor getProfessor() {
		return professor;
	}

	public void setProfessor(Professor professor) {
		this.professor = professor;
	}

}
