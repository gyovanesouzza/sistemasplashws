package com.gyovanesouzza.springboost.beans;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
public class Turma implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	private String anoSerie;
	private String turma;
	
	@Temporal(TemporalType.DATE)
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy", locale = "pt-BR", timezone = "America/Sao_Paulo")
	private Date anoTurma;

	@JsonBackReference
	@OneToMany(mappedBy = "turma", fetch = FetchType.EAGER)
	private Set<Aluno> alunos = new HashSet<Aluno>();

	@JsonBackReference
	@ManyToMany(cascade = CascadeType.ALL, mappedBy = "turmas")
	private Set<Professor> professores = new HashSet<>();

	public Turma() {
	}

	public Turma(Integer id, String ano_serie, String turma, Date ano_turma) {
		super();
		this.id = id;
		this.anoSerie = ano_serie;
		this.turma = turma;
		this.anoTurma = ano_turma;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAnoSerie() {
		return anoSerie;
	}

	public void setAnoSerie(String ano) {
		this.anoSerie = ano;
	}

	public String getTurma() {
		return turma;
	}

	public void setTurma(String turma) {
		this.turma = turma;
	}

	public Date getAnoTurma() {
		return anoTurma;
	}

	public void setAnoTurma(Date ano_turma) {
		this.anoTurma = ano_turma;
	}

	public Set<Aluno> getAlunos() {
		return alunos;
	}

	public void setAlunos(Set<Aluno> alunos) {
		this.alunos = alunos;
	}

	public Set<Professor> getProfessores() {
		return professores;
	}

	public void setProfessores(Set<Professor> professores) {
		this.professores = professores;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Turma other = (Turma) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
