package com.gyovanesouzza.springboost.beans;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Usuario implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	@Column(nullable = false, unique = true)
	private String username;
	
	private String senha;
	@Column(nullable = false, unique = true)
	private String email;
	private String nome;

	@JsonBackReference
	@OneToOne(mappedBy = "usuario")
	private Aluno aluno;
	
	@JsonBackReference
	@OneToOne(mappedBy = "usuario")
	private Professor professor;

	public Usuario() {
	}

	public Usuario(Integer id, String username, String senha, String email, String nome) {
		super();
		this.id = id;
		this.username = username;
		this.senha = senha;
		this.email = email;
		this.nome = nome;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String login) {
		this.username = login;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	

}
