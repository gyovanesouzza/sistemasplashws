package com.gyovanesouzza.springboost.beans;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import org.hibernate.annotations.Cascade;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
public class Aluno implements Serializable {

	private static final long serialVersionUID = 1L;
	private static final String ZERO_FILL = "%06d";
	@Id
	@Column(columnDefinition = "int(6) ZEROFILL")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer rm;
	private String nome;

	@JsonFormat(pattern = "dd/MM/yyyy")
	private Date dt_nascimento;
	private String mae;
	private String pai;
	@Column(nullable = false, unique = true, length = 14)
	private String cpf;
	@Column(nullable = false, unique = true)
	private String rg;
	private Integer idade;
	private String foto;

	@JsonManagedReference
	@ManyToOne
	@JoinColumn(name = "endereco_id")
	private Endereco endereco;

	@JsonManagedReference
	@ManyToOne
	@JoinColumn(name = "telefone_id")
	private Telefone telefone;
	@JsonManagedReference
	@OneToOne
	@JoinColumn(name = "usuario_id")
	private Usuario usuario;

	@JsonManagedReference
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "turma_id")
	private Turma turma;

	@JsonBackReference
	@OneToMany(mappedBy = "id.aluno")
	private Set<Nota> nota = new HashSet<>();

	public Aluno() {
	}

	

	public Aluno(Integer rm, String nome, Date dt_nascimento, String mae, String pai, String cpf, String rg,
			Integer idade, String foto, Usuario usuario) {
		super();
		this.rm = rm;
		this.nome = nome;
		this.dt_nascimento = dt_nascimento;
		this.mae = mae;
		this.pai = pai;
		this.cpf = cpf;
		this.rg = rg;
		this.idade = idade;
		this.foto = foto;
		this.usuario = usuario;
	}

	public Integer getRm() {
		return rm;
	}

	public String getRMZeroFill() {
		return getRm() == null ? null : String.format(ZERO_FILL, getRm());
	}

	public void setRm(Integer rm) {
		this.rm = rm;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getMae() {
		return mae;
	}

	public void setMae(String mae) {
		this.mae = mae;
	}

	public String getPai() {
		return pai;
	}

	public void setPai(String pai) {
		this.pai = pai;
	}

	public Date getDt_nascimento() {
		return dt_nascimento;
	}

	public void setDt_nascimento(Date dt_nascimento) {
		this.dt_nascimento = dt_nascimento;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getRg() {
		return rg;
	}

	public void setRg(String rg) {
		this.rg = rg;
	}

	public Integer getIdade() {
		return idade;
	}

	public void setIdade(Integer idade) {
		this.idade = idade;
	}

	public String getFoto() {
		return foto;
	}

	public void setFoto(String foto) {
		this.foto = foto;
	}


	public Set<Nota> getNota() {
		return nota;
	}

	public void setNota(Set<Nota> nota) {
		this.nota = nota;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public Telefone getTelefone() {
		return telefone;
	}

	public void setTelefone(Telefone telefone) {
		this.telefone = telefone;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Turma getTurma() {
		return turma;
	}

	public void setTurma(Turma turma) {
		this.turma = turma;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((rm == null) ? 0 : rm.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Aluno other = (Aluno) obj;
		if (rm == null) {
			if (other.rm != null)
				return false;
		} else if (!rm.equals(other.rm))
			return false;
		return true;
	}

}
