package com.gyovanesouzza.springboost.beans.enuns;

public enum TipoAtividade {
	CADERNO(1, "Caderno"), AVALICAO(2, "Avalição");

	private TipoAtividade(Integer id, String msg) {
		this.id = id;
		this.msg = msg;
	}

	private Integer id;
	private String msg;

	public Integer getId() {
		return id;
	}

	public String getMsg() {
		return msg;
	}

}
