package com.gyovanesouzza.springboost.beans;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Embeddable
public class NotaPK implements Serializable {

	private static final long serialVersionUID = 1L;
	@JsonManagedReference
	@ManyToOne(cascade = CascadeType.REFRESH)
	@JoinColumn(name = "aluno_id")
	private Aluno aluno;
	@JsonManagedReference
	@ManyToOne
	@JoinColumn(name = "atividade_id")
	private Atividade atividade;

	public NotaPK() {
		// TODO Auto-generated constructor stub
	}

	public NotaPK(Aluno aluno, Atividade atividade) {
		super();
		this.aluno = aluno;
		this.atividade = atividade;
	}

	public Aluno getAluno() {
		return aluno;
	}

	public void setAluno(Aluno aluno) {
		this.aluno = aluno;
	}

	public Atividade getAtividade() {
		return atividade;
	}

	public void setAtividade(Atividade atividade) {
		this.atividade = atividade;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((aluno == null) ? 0 : aluno.hashCode());
		result = prime * result + ((atividade == null) ? 0 : atividade.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NotaPK other = (NotaPK) obj;
		if (aluno == null) {
			if (other.aluno != null)
				return false;
		} else if (!aluno.equals(other.aluno))
			return false;
		if (atividade == null) {
			if (other.atividade != null)
				return false;
		} else if (!atividade.equals(other.atividade))
			return false;
		return true;
	}

}
