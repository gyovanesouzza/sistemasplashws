package com.gyovanesouzza.springboost.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.gyovanesouzza.springboost.beans.Endereco;

public interface EnderecoRepository  extends JpaRepository<Endereco, Integer> {

	
	Endereco findByEstadoAndCidadeAndCepAndBairroAndRuaAndNumeroAndComplemento(String estado,String cidade,String cep ,String Bairro,String rua ,String Numero ,String Complemento);
	
	
}
