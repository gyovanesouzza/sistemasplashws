package com.gyovanesouzza.springboost.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.gyovanesouzza.springboost.beans.Aluno;
import com.gyovanesouzza.springboost.beans.Endereco;

public interface AlunoRepository extends JpaRepository<Aluno, Integer> {

	Aluno findTopByOrderByRmDesc();

	List<Aluno> findByEnderecoAndRmNot(Endereco endereco, Integer id);

}
