package com.gyovanesouzza.springboost.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import com.gyovanesouzza.springboost.beans.Materia;

public interface MateriaRepository  extends JpaRepository<Materia, Integer> {

	
	@Transactional(readOnly = true)
	Materia findByNome(String nome);
}
