package com.gyovanesouzza.springboost.Repository;

import java.sql.Date;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import com.gyovanesouzza.springboost.beans.Turma;

public interface TurmaRepository extends JpaRepository<Turma, Integer> {

	@Transactional(readOnly = true)
	Turma findByTurmaAndAnoSerieAndAnoTurmaAndIdNot(String turma, String anoSerie, Date anoTurma, Integer id);
	@Transactional(readOnly = true)
	Turma findByTurmaAndAnoSerieAndAnoTurma(String turma, String anoSerie, java.util.Date anoTurma);
}
