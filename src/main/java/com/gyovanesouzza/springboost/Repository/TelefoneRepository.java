package com.gyovanesouzza.springboost.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.gyovanesouzza.springboost.beans.Telefone;

public interface TelefoneRepository extends JpaRepository<Telefone, Integer> {

	Telefone findByDddTelefoneAndTelefoneAndDddCelularAndCelular(String dddTelefone, String telefone, String dddCelular,
			String celular);

}
