package com.gyovanesouzza.springboost.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.gyovanesouzza.springboost.beans.Professor;

public interface ProfessorRepository  extends JpaRepository<Professor, Integer> {
	
	Professor findTopByOrderByIdDesc();

}
