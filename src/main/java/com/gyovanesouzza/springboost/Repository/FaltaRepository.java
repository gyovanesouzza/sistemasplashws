package com.gyovanesouzza.springboost.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.gyovanesouzza.springboost.beans.Falta;

public interface FaltaRepository  extends JpaRepository<Falta, Integer> {

}
