package com.gyovanesouzza.springboost.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.gyovanesouzza.springboost.beans.Atividade;

public interface AtividadeRepository  extends JpaRepository<Atividade, Integer> {

}
