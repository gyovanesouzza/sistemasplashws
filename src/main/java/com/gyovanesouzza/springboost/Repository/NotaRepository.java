package com.gyovanesouzza.springboost.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.gyovanesouzza.springboost.beans.Aluno;
import com.gyovanesouzza.springboost.beans.Nota;
import com.gyovanesouzza.springboost.beans.NotaPK;

public interface NotaRepository extends JpaRepository<Nota, Integer> {

}
